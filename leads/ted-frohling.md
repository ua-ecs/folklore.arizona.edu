[ ] I may be exceptional, but that's a completely different topic. :grinning: There are at least 4 others that come to mind of the first generation that are still working at the U. They are Warren VanNest, Joe Gotobed, Skip Schaller and Ric Anderson. I'm sure there are others that I'm forgetting.

[ ] Don't forget the spider net was there before the TIPS project put in the first generation fiber plant on campus. Before the spider net, the Computer Science dept was on the 3rd floor of the Computer Center and to send email to anyone else on the campus the traffic went to Wisconsin so it could be converted to the Bitnet IBM network so the IBM mainframe on the second floor of the Computer Center could receive the email.

[ ] Don't forget the Morris Sendmail worm setup the circumstances that lead to the formation of the Netmgrs group.
