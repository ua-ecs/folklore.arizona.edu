
Hi!

My name is Mark Fischer, and I work at UITS here at the U of A. I'm not sure we've ever met but I've certainly heard your name around for many years.

I'm starting a project to collect folklore from the early days of IT here at the U of A and your name has come up a lot as someone I should ask for stories, so I'm asking for stories. :)

https://discourse.uits.arizona.edu/t/collecting-it-folklore/1357

I'm just starting out on this project, so I don't really know how to proceed exactly, so first I just wanted to introduce myself and see if you'd be interested in participating.

Thanks!

- Mark





I'm open to different formats of collecting the story. If you want to just write something up and email it to me that's great, or we can sit down and have you tell it in person and I can record it for transcription? Its also been suggested that I organize a small group of people and talk over lunch, or in 303, or if you have another idea on how to get the story archived I'm open to ideas.

Thanks!

- Mark


