Well not so fast!
 
I still have a few more weeks before I jettison the mothership.
 
[ ] How about the LAN wars?:
[ ] Novel net
[ ] AT&T Starnet
[ ] Lantastic
 
[ ] Then the evolution of packet switching using dumb terminals to PCs.
[ ] The PC vs. Mac/Apple wars.
 
[ ] The media evolution;
[ ] RS-232 packet switching multiplexed over T1 circuits all over campus.
[ ] Then the Thick net, Thin net and twisted pair Ethernet net.
[ ] The Engineering broadband network from Engineering was a big player.
 
[ ] Card catalogs at the Library replaced with the OCLC on line card catalog system.
 
[ ] Punch card registration to Touch Tone telephone registration and then on to Web based services.
 
[ ] College of Agriculture County Office connectivity.
 
[ ] UA South Offices in Southern Arizona
 
[ ] The Tech Park, from the Old Pueblo Club building downtown to the Rita Ranch IBM Facility.
 
[ ] The Arizona State Public Information Network (APSIN) using the UofA as the Hub for the southern half of the state.
[ ] All public Local and State agencies could hook up to the Ethernet.
 
[ ] How about those Ethernet service rates?
[ ] I was the one that stood in front of the Net Manager meeting and announced the price of a 100BaseT connection was going to be $1000.00 per drop not including the station cable upgrade costs…
 
[ ] But there were good efforts made in delivering reliable service such as redundant campus network backbone.
 
[ ]  E-Team, Ted, Warren V. and myself to explain what Ethernet was and how it could be used…
 
[ ] Satellite dish connections to super computer facilities like the John Van Neuman National Supercomputer Center.
 
[ ] And in recent times;
[ ] Campus wide Wi-Fi.
[ ] High speed connectivity to the Mt Graham Observatory, Tummamoc Hill, and other places.
[ ] 40 gig and more…
 
 
[ ] And yes the battles of the Network titans. (Long list of names not to be mentioned.)
[ ] Evolutions and changes in the Telecommunications department.
[ ] The Computer Center metamorphosis as IT and technology changed and was intertwined in the way the University grew and continues to grow.
 
[ ] And there is yet more related to Desk Top Support, Administrative Data Processing, OSCR and much more.
 
It’s been a fun 33 year ride.
 
 