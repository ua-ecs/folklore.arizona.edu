[ ] Definitely get the Hanley Strapman story from Warren and Ric, though I think that was 80s so maybe not as old as you’re looking for, and not real “computer related” per-say, but it’s the best.

[ ] Warren and/or Ric also had some anecdote about the washing machine-sized disks for one of the early central computers getting out of balance and “walking” around the data center. On the Prime maybe? Or the CDC? Can’t remember the details.

[ ] Might want to drop an email to Mike Torregrossa, I think he’s still over at Alumni. If I remember right, he used to run punch cards and result printouts to/from one of the first computers on campus in the Physics department to the engineering department. I seem to recall him telling me about having to take crazy routes between the buildings so grad students wouldn’t ambush him for their results in the middle of campus so they could get a jump on making new card stacks for the next run. I believe Joellen was an operator in Physics at that time, even, if I remember correctly.

[ ] I can’t remember any specifically, but I seem to recall Jason at AZPM relaying some stories he heard from Cheech Calenti. I believe Cheech was class of Rob Macarthur in staff time so that might be, like, Generation 0 I guess.

[ ] I don’t know if you know Joe Fico but he might have some good stories from the beginnings of HACKS when there were limited ways for undergrads to get access to computers. I think that’s late 80s/early 90s. The HACKS constitution was ratified in Feb of ’90 so sometime around then. Chd was part of that also, I think.

[ ] And I’d probably get an email to Walt Moody, he could/would tell you many tales, I expect.

[ ] Kathleen also maybe? I’m pretty sure she had Todd take her off every possible listserv so I doubt she saw this.

Cheers,
Adam
