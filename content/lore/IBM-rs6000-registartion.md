---
title: IBM RS/6000 Registration System
author: Gary Windham
date: "2017-04-26T10:00:00-07:00"
tags:
  - IBM
  - RS/6000
  - Vocom
  - Dialogic
  - rsvp
  - pots
  - 1990s
  - 1993
people:
  - Gary Windham
places:
---

In 1993, UA hired the company I was working for (The Robinson Group) to replace the Vocom/Dialogic-based telephone registration system (RSVP or ""Registration System Via Phone") with an IBM DirectTalk/6000-based system running on IBM RS/6000 hardware. 

<!--more-->

It replaced the individual POTS connections with 4 T1 connections to the 5ESS, allowing a whopping 94 concurrent calls (we reserved 2 of the 96 channels for testing). We also implemented the first automated, self-service credit card payment system for tuition and fees--using 4 x 2400bps modems. Finally, the most radical departure was moving from the SNA/3270 communications protocols used by the Vocom to a 3270 HLLAPI ("high-level language API") emulation running over TCP/IP.

--Gary
