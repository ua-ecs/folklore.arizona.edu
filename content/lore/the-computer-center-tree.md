---
title: The Computer Center Tree
author: Warren Van Nest
date: "2017-04-12T09:00:00-07:00"
images: [ "http://folklore.arizona.edu/images/computer-center-tree.jpg" ]
tags:
  - trees
  - speedway
  - 1990s
places:
  - Computer Center
  - 1702
---

There used be a very large Aleppo pine tree in a grassy area between the northeast side of the Computer Center and Speedway Boulevard. Then around 1990 work began on a major Speedway widening project which would expand the roadway from four lanes to six lanes and create the three UofA pedestrian underpasses.  

The 95 year old, 65 foot tall tree was in the way and had to go.  To save it, the UofA dug it up and relocated it to a large circular planter just west of the Warren Avenue underpass over by where Eric’s Ice Cream was (now 1702) where it happily lives today.  That was one big tree to move!

If my memory serves me correctly, this same Speedway widening project also put in place the infrastructure which allowed us, for the first time, to extend the campus fiber optic and twisted-pair cable infrastructure from the main campus to the north of Speedway.  Prior to that our options for crossing Speedway were limited to microwave or a rediculously long metallic path on USWest Telco facilities all the way down to their downtown CO and back to reach the other side of the street.

The next time you walk east on Speedway by the Warren Ave. underpass, stop and look up at the “Computer Center Tree” (see picture).

{{< figure src="/images/1990s/computer-center-tree.jpg" title="Computer Center Tree" >}}

> In more recent UofA history, four Aleppo pines near McKale Center were relocated in 2007, each weighing an estimated 90,000 lbs.  http://tucsoncitizen.com/morgue/2007/05/05/50686-moving-90-000-lb-trees/

-- Warren
