---
title: Touch-Tone Phone Registration
author: David Murphy
date: "2017-04-26T09:45:00-07:00"
images: [ "http://folklore.arizona.edu/images/1980s/registration-punch-card.png" ]
tags:
  - touch tone phone
  - registration
  - punch cards
  - 1986
  - 1980s
  - BYU
---

Back in 1986 the University registration process was based on stacks of punch cards. Each punch card was a seat in a class. You gather a card for each class and turn them in to registration under your name…

<!--more-->

{{< figure src="/images/1980s/registration-punch-card.png" title="Punch Card used for Course Registration" >}}

Then class lists were handed out to the profs and they knew who was in their class.
Billing and other functions were all based on the cards…
 
So, the first half of 1986 Pete Perona, Pam Stevens(Roman), Patti Fastje and I went to BYU to look at a new automated telephone based registration system.

Here’s a note on how the system was configured.

{{< figure src="/images/1980s/Voice-Registration.png" title="IDX 3000 Hex Conversion Chart" >}}
