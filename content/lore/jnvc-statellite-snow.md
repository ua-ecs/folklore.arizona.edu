---
title: Satellite Link Under the Weather
author: Ric Anderson
date: "2017-05-08T11:48:00-07:00"
images: [ "http://folklore.arizona.edu/images/2000s/computer-center-roof-2008-thumb.jpg" ]
tags:
  - satellite
  - snow
  - network
  - 1980s
  - 1987
places:
  - computer center
---

In the late 80s[^1] the University of Arizona had a satellite link to the [John von Neumann
Center][3] for our internet connectivity. The link itself was a 57Kbit satellite connection
using that giant roof mounted antenna on top of the computer center.

One morning the [VitaLink Corp][2] Satellite NOC called us to see if we had lost power as they 
had lost signal from us.

<!--more-->

{{< figure src="/images/2000s/computer-center-roof-2008.jpg" title="Satellite on the roof of the Computer Center" >}}

It turns out we had some snow that morning and the dish collected a bunch of it.  It
turned out our dish didn't have the "cold weather package" (a heater) and thus was "down"
until the snow melted of its own accord.

We sent photos by surface mail (no digital cameras) to VitaLink - they could not believe
Tucson got snow at all, never mind enough snow to take out their dish.

[^1]: JNVC was only around from 1985-1990 so I'm using that as a time frame

[2]: http://www.historyofcomputercommunications.info/Book/12/12.28_Vitalink.html
[3]: https://en.wikipedia.org/wiki/John_von_Neumann_Center

Ric

> Editor's note.  There was a large snowfall on Christmas 1987 in Tucson, this might be 
> the event in question.

