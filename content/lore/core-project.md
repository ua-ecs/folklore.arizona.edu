---
title: Continuous Organizational Renewal (CORe)
author: David Murphy
date: "2017-04-26T09:30:00-07:00"
images: [ "http://folklore.arizona.edu/images/1990s/CORE-article-thumb.png" ]
tags:
  - core
  - 1990s
  - 1993
people:
  - David Murphy
---

Continuous Organizational Renewal was the Intel internal process they used to operate Intel.

<!--more-->

The article was March 1st, 1993

That project was 3 months and finished up in June of 93.

Just in time inventory, cycle time reduction, etc…
 
We won an award from the Governor’s office.


{{< figure src="/images/1990s/CORE-article.png" title="CORe Article in the Daily Wildcat: March 1st 1993" >}}
