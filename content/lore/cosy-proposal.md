---
title: CoSy Proposal Paper
author: Warren Van Nest
date: "2017-04-21T09:00:00-07:00"
images: [ "http://folklore.arizona.edu/images/1980s/cosy-paper.png" ]
tags:
  - cosy
  - instructional computing
  - communication
  - 1980s
people:
  - Roger Caldwell
  - Mely Tynan
  - Bob Leach
  - Warren Van Nest
  - Ted Frohling
  - Roger Caldwell
---

A few people have mentioned CoSy which was an early “computer conferencing / communication system” implemented by CCIT (now UITS) back in 1987.  It later branched out into Instructional CoSy which was used by students and their professors for communication.  

I found a scan of an old white paper on the first phase of the CoSy project that Roger Caldwell, Mely Tynan, Bob Leach, and I put together.  It’s got miscellaneous references to history / environment back in those days (30 years ago!), as well as a window into project planning and rollout considerations at that time.  One thing that I remember is how much attention we spent toward achieving critical mass, a factor that can make or break the successful adoption of many communication methods.

The paper mentions fun items like campus size, and that the University had about 4000 microcomputers (PC’s), 2100 terminals, and about 1000 modems.  “A number of faculty, staff, and students also owned private units” [personal computers].  So this was back in the fairly early days of individuals acquiring personal computers either at work or at home.  Under “Checklist for Successful Implementation” there are a few interesting tidbits, like the importance of having convenient daily access to equipment (terminals or PCs) so that you can access the system.

After the initial rollout and adoption by campus, I remember Ted Frohling getting heavily involved with coding local customizations to the product to fix or enhance services of importance to the campus.

Anyway, this paper might be fun to look through for a 30 year old reflection of part of CCIT / Telecom / UofA history.  Oh, and it’s also interesting to see the commonly used fonts and output that we used back then compared to the much cleaner / prettier output produced by the modern tools we take for granted today.

Paper attached.

cheers,

Warren

{{< figure src="/images/1980s/cosy-paper.png" link="/images/1980s/CoSy-Paper.pdf" class="file" title="CoSy Proposal PDF" >}}
