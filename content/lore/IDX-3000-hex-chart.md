---
title: IDX 3000 Hex Chart
author: David Murphy
date: "2017-04-26T09:30:00-07:00"
images: [ "http://folklore.arizona.edu/images/1980s/HEX-conversion-thumb.png" ]
tags:
  - IDX 3000
  - terminals
  - VT-100
  - 1980s
places:
  - Computer Center
  - Copper Cable Plant
---

Just in case someone needs to know what the value of decimal (75) is in Hexadecimal (4B) or Binary (0100 1011).

This was something you needed to know to manage the IDX-3000 x.25 Packet switch.
3000 was the number of Async. ports a single IDX processor could support.
Later IDX created a way to link two IDX-3000 processors together.

We used the IDX-3000 to distribute Asynchronous based terminals across the campus.
This was parallel to the  Engineering Sytek and replacing the Computer Center PAC-X packet switch.

<!--more-->

Prime Computer and other async. multiport central computer ports were attached to the IDX-3000.

From the IDX central switch/hub in the computer center, channelized T1’s were extended across campus on our campus copper cable plant. At the remote end of the IDX T1 was a 24 port MUX with DB25 Async. ports. VT100 terminals were attached to the MUX. Terminal ports were mapped to a pool of computer ports so there was some degree of efficient computer port utilization. Kind of like time division multiplexing.
 
Central computer Applications were designed to respond to key strokes from a VT-100 (QWERTY) keyboard or something emulating a VT-100 terminal. Users would hit the enter key a couple of times, the IDX would connect the terminal to an available computer port, the computer would present a Login prompt on the terminal screen, the user would log into their account over the IDX-3000 network.

{{< figure src="/images/1980s/HEX-conversion.png" title="IDX 3000 Hex Conversion Chart" >}}
