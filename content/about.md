---
title: About this site
date: "2017-04-08T09:00:00-07:00"
tags:
  - about
people:
  - Mark Fischer
  - Todd Merritt
  - Regina Watkins
places:
  - 1702
---

Contributors
============

Do you have a story you'd like to share? Submit it as an issue on this site's git repository:

https://bitbucket.org/ua-ecs/folklore.arizona.edu/issues

The idea for this site came about at 1702 during the retirement party for Regina Watkins
in the spring of 2017. Todd Merritt was commenting that within the next 5 years the first
generation of IT staff at the University of Arizona will almost all be gone. And so, the
idea for a site to collect the folklore of computing at the University of Arizona.


This site was originally built by Mark Fischer in April of 2017.

