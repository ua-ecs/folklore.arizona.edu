---
title: Red Punch Card
author: David Murphy
date: "2017-04-19T09:00:00-07:00"
thumbnail: "/images/1980s/red-punch-card.png"
images: [ "http://folklore.arizona.edu/images/1980s/red-punch-card.png" ]
tags:
  - punch card
  - 1980s
---

What's the significance of a red punch card in a run deck?

Hint: It has nothing to World of Warcraft...

It's a job card, the first card in the deck to be read by the punch card reader.

<!--more-->

{{< figure src="/images/1980s/red-punch-card.png" title="Computer Center Red Punch Card" >}}
