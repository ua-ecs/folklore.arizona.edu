---
title: UANet 1980s
author: David Murphy
date: "2017-04-19T09:00:00-07:00"
thumbnail: "/images/1980s/UANet.png"
images: [ "http://folklore.arizona.edu/images/1980s/UANet.png" ]
tags:
  - network
  - uanet
  - 1980s
---

Images of the UANet from 1987

<!--more-->

{{< figure src="/images/1980s/UANet.png" title="UANet Diagram November 1987" >}}

{{< figure src="/images/1980s/UANet-external.png" title="UANet November 1987 with plans for additional external connections" >}}
