---
title: IBM 3274 Configuration Card
author: David Murphy
date: "2017-04-26T09:00:00-07:00"
thumbnail: "/images/1980s/3270-config-card-thumb.png"
images: [ "http://folklore.arizona.edu/images/1980s/3270-config-card-thumb.png" ]
tags:
  - IBM
  - 3274
  - 1980s
  - 1985
---

Here’s another chapter of ADP-CCIT-UITS

We went thru a IBM “Big Iron” phase.

Here’s a 3274 controller configuration card used to program the remote “multiplexers”.

Part of the IBM SNA 3270 distributed communications architecture.

They had both channel  and T1 attached device controllers.
 
The UA was the first user of the IBM 7171 Async. Controller west of the Mississippi.

<!--more-->

{{< figure src="/images/1980s/3270-config-card.png" title="IBM 3274 Configuration Card" >}}
