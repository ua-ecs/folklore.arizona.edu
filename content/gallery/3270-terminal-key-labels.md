---
title: 3270 Terminal Key Labels
author: David Murphy
date: "2017-04-26T09:00:00-07:00"
thumbnail: "/images/1980s/3270-key-labels-thumb.png"
images: [ "http://folklore.arizona.edu/images/1980s/3270-key-labels-thumb.png" ]
tags:
  - IBM
  - 3270
  - 1980s
---

How about the IBM 3270 “dumb” terminal key labels.

<!--more-->

{{< figure src="/images/1980s/3270-key-labels.png" title="IBM 3270 Terminal Key Labels" >}}
