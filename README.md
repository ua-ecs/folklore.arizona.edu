# IT Folklore at the University of Arizona

This is the git repository for the http://folklore.arizona.edu website.

See http://folklore.arizona.edu/about for more information.

To contribute a story, please submit an issue here, or email it-folklore@list.arizona.edu

